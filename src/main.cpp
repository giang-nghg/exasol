#include <iostream>
#include <iomanip>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <string>
#include <openssl/sha.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <future>
#include <chrono>
#include <atomic>

using boost::asio::ip::tcp;
namespace ssl = boost::asio::ssl;
namespace asio = boost::asio;
using namespace std;

struct Result
{
  bool found = false;
  unsigned long long suffix = 0;
  unsigned char hash[SHA_DIGEST_LENGTH];
};

struct Thread
{
  future<Result> handle;
  bool finished = false;
};

atomic<bool> done (false);

string hash_bytes_to_hex_str(const unsigned char hash[])
{
  stringstream hex_ss;
  hex_ss << setfill('0') << hex;
  for (int i = 0; i < SHA_DIGEST_LENGTH; i++)
  {
    hex_ss << ((hash[i] & 0xf0) >> 4) << (hash[i] & 0x0f);
  }
  return hex_ss.str();
}

Result check_hash(const string& auth_data, unsigned long long suffix, int difficulty)
{
  unsigned char hash[SHA_DIGEST_LENGTH];
  string message = auth_data + to_string(suffix);
  SHA1(reinterpret_cast<const unsigned char*>(message.c_str()), message.size(), hash);

  int n_zero_bytes = difficulty / 2;
  bool odd_byte = (bool)(difficulty % 2);

  bool satisfied = false;
  int i;
  for (i = 0; i < n_zero_bytes; i++)
  {
    if (hash[i] != 0x00)
      break;
    else
      continue;
  }
  if (i == n_zero_bytes)
    satisfied = true;
  if (odd_byte)
  {
    satisfied &= hash[i] < 0x10;
  }
  
  Result result = { .found = satisfied, .suffix = suffix };
  for (int i = 0; i < SHA_DIGEST_LENGTH; i++)
    result.hash[i] = hash[i];
  return result;
}

void write(const char message[], ssl::stream<tcp::socket>& socket)
{
  asio::streambuf req_buf;
  ostream req_stream(&req_buf);
  req_stream << message << endl;
  asio::write(socket, req_buf);
}

void write_info(const string& info, const string& auth_data, const string& arg_1, ssl::stream<tcp::socket>& socket)
{
  string msg_to_hash = auth_data + arg_1;
  unsigned char hash[SHA_DIGEST_LENGTH];
  SHA1(reinterpret_cast<const unsigned char*>(msg_to_hash.c_str()), msg_to_hash.size(), hash);
  string hex_str = hash_bytes_to_hex_str(hash);
  write((hex_str + " " + info).c_str(), socket);
}

Result check_hash_for_result(const string& auth_data, unsigned long long start, unsigned long long end, int difficulty)
{
  for (unsigned long long suffix = start; suffix < end; suffix++)
  {
    if (done.load())
      break;

    Result result = check_hash(auth_data, suffix, difficulty);
    if (!result.found)
      continue;
    else
    {
      cout << "Found (in thread): " << time(NULL) << endl;
      return result;
    }
  }
  return Result { .found = false };
}

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 3)
    {
      cerr << "Usage: client <host> <port>\n";
      return 1;
    }

    asio::io_context io_context;

    tcp::resolver resolver(io_context);
    tcp::resolver::results_type endpoints =
      resolver.resolve(argv[1], argv[2]);

    ssl::context ctx(ssl::context::sslv23);
    ctx.load_verify_file("auth/ca.pem");
    ctx.use_certificate_file("auth/cert.pem", ssl::context::pem);
    ctx.use_rsa_private_key_file("auth/rsa.pem", ssl::context::pem);

    ssl::stream<tcp::socket> socket(io_context, ctx);
    asio::connect(socket.lowest_layer(), endpoints);
    socket.handshake(boost::asio::ssl::stream_base::client);

    string auth_data;
    for (;;)
    {
      asio::streambuf resp_buf;
      boost::system::error_code error;
      asio::read_until(socket, resp_buf, '\n', error);

      if (error == asio::error::eof)
        break; // Connection closed cleanly by peer.
      else if (error)
        throw boost::system::system_error(error); // Some other error.

      istream resp_stream(&resp_buf);
      string resp;
      resp_stream >> resp;
      cout << resp << endl;

      if (resp.compare("HELO") == 0)
      {
        write("EHLO", socket);
      }
      else if (resp.compare("ERROR") == 0)
      {
        string server_err_msg;
        while (!resp_stream.eof())
        {
          string tmp_str;
          resp_stream >> tmp_str;
          server_err_msg.append(" " + tmp_str);
        }
        cout << server_err_msg << endl;
        break;
      }
      else if (resp.compare("POW") == 0)
      {
        int difficulty;
        resp_stream >> auth_data >> difficulty;
        cout << auth_data << " " << difficulty << endl;

        Result result;
        constexpr int n_threads = 7;
        unsigned long long suffixes_per_thread = (ULLONG_MAX-1) / n_threads;
        Thread handles[n_threads];
        cout << "Start: " << time(NULL) << endl;
        for (int thread = 0; thread < n_threads; thread++)
        {
          unsigned long long start = thread * suffixes_per_thread;
          unsigned long long end = start + suffixes_per_thread;
          handles[thread].handle = async(launch::async, check_hash_for_result, auth_data, start, end, difficulty);
        }

        int finished_count = 0;
        while (!result.found && finished_count < n_threads)
        {
          for (int thread = 0; thread < n_threads; thread++)
          {
            if (!handles[thread].finished && handles[thread].handle.wait_for(chrono::nanoseconds(1)) == future_status::ready)
            {
              result = handles[thread].handle.get();
              handles[thread].finished = true;
              finished_count++;
              if (result.found)
              {
                cout << "Found (in loop): " << time(NULL) << endl;
                done.store(true);
                break;
              }
            }
          }
        }
        if (!result.found)
        {
          result = check_hash(auth_data, ULLONG_MAX - 1, difficulty);
        }
        if (!result.found)
        {
          result = check_hash(auth_data, ULLONG_MAX, difficulty);
        }

        if (!result.found)
        {
          cout << "Exhausted" << endl;
        }
        else
        {
          cout << result.suffix << " " << hash_bytes_to_hex_str(result.hash) << endl;
          cout << "Write: " << time(NULL) << endl;
          write(to_string(result.suffix).c_str(), socket);
        }
      }
      else if (resp.compare("END") == 0)
      {
        write("OK", socket);
        break;
      }
      else
      {
        string info = "";
        if (resp.compare("NAME") == 0)
          info = "Giang Nguyen";
        else if (resp.compare("MAILNUM") == 0)
          info = "1";
        else if (resp.compare("MAIL1") == 0)
          info = "giang.nghg@gmail.com";
        else if (resp.compare("SKYPE") == 0)
          info = "giang.nghg@gmail.com";
        else if (resp.compare("BIRTHDATE") == 0)
          info = "27.12.1991";
        else if (resp.compare("COUNTRY") == 0)
          info = "Vietnam";
        else if (resp.compare("ADDRNUM") == 0)
          info = "2";
        else if (resp.compare("ADDRLINE1") == 0)
          info = "11/6D To Ky street, My Hue hamlet, Trung Chanh ward";
        else if (resp.compare("ADDRLINE2") == 0)
          info = "Hoc Mon district, Ho Chi Minh city";

        if (info.compare("") != 0)
        {
          string arg_1;
          resp_stream >> arg_1;
          cout << arg_1 << endl;
          write_info(info, auth_data, arg_1, socket);
        }
      }
    }
  }
  catch (exception& e)
  {
    cerr << e.what() << endl;
  }

  return 0;
}